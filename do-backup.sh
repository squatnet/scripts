#!/bin/sh

########################
# Read the corresponding docs before using this script
########################

# taken from https://git.tails.boum.org/puppet-tails/tree/files/borgbackup/runbackupfs.sh
# https://git.tails.boum.org/puppet-tails/tree/manifests/borgbackup/fs.pp
# And https://gist.github.com/daniel-werner/5ab30d2e5c566adaad3022f4da9e141d for mysqldump
# and some help from anarcat

# Exit on error
set -e

# Space-separated list of directories to backup.
# Will you the value in the env or "/" by default.
# Borg might be started with --one-file-system, so list here all filesystems to backup
DIRECTORIES="${DIRECTORIES:-/}"

# Where should DB backups be stored
DBOUTPUTDIR='/var/backups/mysql/'

# By default we are verbose
QUIET=0

# Use BORG_OPTS from env if available
# Options meanings:
# --list: display the list of crawled files
# --filter=E: but only if an error happened
# --progress: show progress (should be disabled when the script runs with cron)
# --one-file-system: do not cross filesystems borders (but respect $DIRECTORIES)
# --exclude-caches: do not copy dirs that has a CACHEDIR.TAG file
BORG_OPTS="${BORG_OPTS:-"--list --filter=E --one-file-system --exclude-caches"}"

# Print stuff on the screen, with date & time
say() { if [ "${QUIET}" -eq 0 ]; then echo >&2 "$(/bin/date +%Y-%m-%dT%H:%M) $1"; fi }
err() { echo >&2 "$(/bin/date +%Y-%m-%dT%H:%M) $1"; }

# Explain how it works
usage() {
  cat <<-EOF
	Usage:
	  With MySQL databases export:
	  DBPASS=<database password> $(basename "$0") -h <backup server> -r <repo name> -u <db username>
	    -h <backup server>: HostName used in ~/.ssh/config for the backup server
	    -r <repo name>: name of the borg repository to use
	    -u <db username>: username to use to dump MySQL database.
	    -q: be quiet: do not display messages

	  Without databases export:
	  NO_DB_EXPORT=1 $(basename "$0") -h <backup server> -r <repo name>
	EOF
  exit 2
}


# First ":" means "don't throw errors" (rather, errors will end up in the ":" case)
# a ":" after a letter means "this arg needs a parameter"
while getopts ":r:qu:h:" option
do
  case "${option}"
  in
    r) REPO=${OPTARG};;
    q) QUIET=1;;
    u) DBUSER=${OPTARG};;
    h) BKSERVER=${OPTARG};;
    \?) usage;;
    :) err "-${OPTARG} requires an argument."; usage;;
  esac
done

if [ -z "$REPO" ]; then
  err "Missing argument: -r <repo name>"
  usage
fi

if [ -z "$BKSERVER" ]; then
  err "Missing argument: -h <backup server>"
  usage
fi

# If NO_DB_EXPORT is not set and $DBUSER is not set either ("-n" = "! -z")
if [ -z "${NO_DB_EXPORT}" ] && [ -z "$DBUSER" ]; then
  err "Missing argument: -u <db username>"
  usage
fi

# If NO_DB_EXPORT is not set and $DBPASS is not set either
if [ -z "${NO_DB_EXPORT}" ] && [ -z "$DBPASS" ]; then
  err "Missing DBPASS env variable"
  usage
fi

say "Starting backup script"

# DB export?
# Just set NO_DB_EXPORT to any value before running script
if [ -z "${NO_DB_EXPORT}" ]; then
  MYSQL='/usr/bin/mysql'
  MYSQLDUMP='/usr/bin/mysqldump'

  say "Getting a list of databases"

  # XXX: Catch wrong password error instead of letting `set -e` ragequit

  # This is possibly insecure: mysql database names can have very wild names with shell 
  # escape sequences and so on. It could totally backfire and allow someone with CREATE 
  # DATABASE privileges in mysql to possible do nasty stuff here.
  # → But we don't create dbnames with weird caracters, so there should be no problem.
  databases=$(${MYSQL} --skip-column-names --user="${DBUSER}" --password="${DBPASS}" -e "SHOW DATABASES;" | \
    grep -Ev "^(performance_schema|information_schema|mysql)$")

  mkdir -p "${DBOUTPUTDIR}"
  
  # dump each database in turn
  for db in $databases; do
    say "Backing up database '${db}'"
    # Here, a hostile database name gets code execution, and we can't really avoid it,
    # so better make sure we don't have weird db names.
    $MYSQLDUMP --force --opt --single-transaction --user="${DBUSER}" --password="${DBPASS}" --databases "${db}" > "${DBOUTPUTDIR}/${db}.sql"
  done
else
  say "Skipping databases export"
fi

say "Starting borg stuff"

# borg will need the passphrase to do its thing
export BORG_PASSCOMMAND="/bin/cat /root/borg-stuff/${REPO}.passphrase"

say "Checking whether the repo exists."
if /usr/bin/borg list "${BKSERVER}:./${REPO}" 2>&1 |grep -qs 'does not exist'; then
  err "Repo '${REPO}' doesn't exist or wrong passphrase (or something else). Exiting."
  exit 1
else
  say "Repo exists."
fi


EXCLFILE="/root/borg-stuff/${REPO}.excludes"
if [ -f "${EXCLFILE}" ]; then
  BORG_OPTS="--exclude-from ${EXCLFILE} ${BORG_OPTS}"
fi

say "Creating new archive on ${BKSERVER}"

# No need to exit on error anymore, better if we catch it
set +e

# The borg magic itself. It writes its output to stderr, so let's redirect that to stdout
/usr/bin/borg create ${BORG_OPTS} "${BKSERVER}:./${REPO}::{hostname}-{now:%Y-%m-%dT%H:%M}" ${DIRECTORIES} 2>&1

# Store borg return code
rc=$?

# Borg return codes: 1=warnings
if [ $rc -eq 1 ]; then
  say "Warnings happened during archive creation"
fi

# Borg return codes: 2=errors
if [ $rc -eq 2 ]; then
  err "Error while creating archive"
fi

# Other borg RCs
if [ $rc -gt 128 ]; then
  ks=$(($rc-128))
  err "Borg was killed during execution by kill signal ${ks}"
fi

# Success
if [ $rc -eq 0 ]; then
  say "Archive successfully created"
fi

say "End of backup script"
